# Tugas Akhir GRAFKOM 2022

## Link Repository
Link Repository: https://gitlab.com/Jsaonn/proyek-grafkom

## Deskripsi Tugas


Program dibuat berbasis web dan dapat menampilkan sebuah scene dan bentuk-bentuk hierarki menggunakan WebGL2. Program dapat menampilkan objek hierarki Human, Dog and Ant. Selain 3 model hierarki scene juga memuat 2 objek yaitu kursi dan meja. Tampilan dapat diubah menjadi 2 mode yaitu dog-view dan default-view. Selain itu, bagian-bagian pada model dapat digerakan dan dianimasikan. Program juga menerapkan Point-light yang dapat diatur. Model juga sudah diterapkan texture dan shading.



# Requirement
* Browser support WebGL
* Node JS to run the server
* vs-code dengan extension live server

## How to Run Program
1. Masuklah ke directory project ini.
2. Jalankan command berupa ```http-server``` pada command prompt. Jika belum memiliki http-server, lakukan instalasi http-server dengan menggunakan command ```npm install http-server -g```.
3. Server NodeJS akan dijalankans secara otomatis.
4. Bukalah browser dan ketikkan laman 127.0.0.1:8080 pada browser.
5. Selamat menjalankan aplikasi Ini.



## Functionality Help
1. Menus <br>
    Apabila user ingin menonaktifkan salah satu fitur seperti ``lighting, animation, dan texture``
2. Point Light<br>
    User dapat mengatur tempat sumber cahaya point source (x,y,z) dan user dapat mengatur warna cahaya (R,G,B)
3. Camera Mode<br>
    User dapat memilih 2 mode camera yaitu ```default``` dan ```dog```. Pada saat mode default user dapat mengatur ```Look At camera Control```
4. Look At Camera Control <br>
   Look At Camera Control adalah fungsi-fungsi untuk mengatur look at pada kamera. Memliki beberapa kegunaan yaitu:
    - Eye Control, terbagi menjadi 3 slider yaitu eye-x, eye-y, dan eye-z untuk mengatur posisi mata.
    - Center Control, terbagi menjadi 3 slider yaitu center-x, center-y, dan center-z untuk mengatur posisi titik pusat yang diamati.
    - Up control, terbagi menjadi 3 slider yaitu up-x, up-y, dan up-z untuk mengatur *upward direction*.
5. Ambient light<br>
    user dapat mengatur warna dari ambient light(R,G,B)
6. Mode <br>
    user dapat memilih 2 mode yaitu ```shading``` dan ```wireframe```
7. Material <br>
    user dapat memilih material untuk model human, ant , dog dan juga untuk ruangan (room)
8. Control <br>
    user dapat memilih objek mana yang ingin dilihat control dari animasi dengan memilih di ```animal control```
   Saat mode animasi dimatikan user dapat menggerakan part-part dari model hierarki yang dipilih di ```animal control```


## Proses Pembentukan Object:
* Membuat kerangka sesuai dengan bentuk bentuk objek (Cube, Cylinder, Sphere)
* Penggabungan objek-objek kedalam bentuk tree dengan meng-translate dan scaling

## Proses Render Object:
* Object dirender dengan cara mentraverse node-node yang ada pada tree

## Fasilitas WebGL yang digunakan:
* Pembentukan kerangka object  dibuat sebuah fungsi agar  ketika ada objek yang memiliki bentuk yang sama,objek hanya perlu di translate dan scale

## Algoritma Khusus yang digunakan:
* Penggunaan struktur data tree untuk model hierarki.
* Pembentukan objek dari kerangka objek
* Pembentukan bayangan pada fragment shader.


## Author
Fasilkom Grafika Komputer - Tugas Akhir - Orca
* Aimar Fikri Salafi - 1906400186
* Johanes Jason - 1906293120

## Pembagian Log tugas
* Aimar Fikri Salafi - 1906400186
  * Membuat model Human and Dog
  * Membuat camera view (default dan sesuai salahsatu pov model berhierarki)
  * Membuat mode wireframe
  * Menerapkan texture pada objek-objek  
    
* Johanes Jason - 1906293120
  * Membuat model Ant, Desk and Chair
  * Melakukan Styling pada tampilan HTML
  * Menerapkan texture pada objek-objek  



## Reference
Template dan basis dari project ini merupakan code yang disediakan di scele.<br>
https://scele.cs.ui.ac.id/mod/resource/view.php?id=103463
