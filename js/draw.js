var animating = 1;

var lightSourceNode;
var roomNode;

var baseDogNode; var baseDogAngle = 0;
var neckNode; var neckAngle = 0; var neckDirection = 0.1;
var headDogNode; var headDogAngle = 0; var headDogDirection = 1;
var eye1Node; var eye1Translation = 0.5; var eye1Direction = 1;
var eye2Node; var eye2Translation = -0.6; var eye2Direction = 1;
var firstEarsNode; var firstEarsAngle = 0; var firstEarsDirection = 1;
var secondEarsNode; var secondEarsAngle = 0; var secondEarsDirection = 1;
var tailBaseNode; var tailBaseAngle = -1.5; var tailBaseDirection = 1;
var tailTopNode; var tailTopAngle = 0; var tailTopDirection = 1;
var firstLegBaseNode; var firstLegBaseAngle = 2.792526803190927; var firstLegBaseDirection = 1;
var secondLegBaseNode; var secondLegBaseAngle = 2.792526803190927; var secondLegBaseDirection = 1;
var thirdLegBaseNode; var thirdLegBaseAngle = 0.6108652381980153; var thirdLegBaseDirection = 1;
var fourthLegBaseNode; var fourthLegBaseAngle = 0.6108652381980153; var fourthLegBaseDirection = 1;

var baseHumanNode; var baseHumanAngle = 4.6600291028248595; var baseHumanDirection = 1;
var humanNoseNode; var humanNoseTranslation = 0; var humanNoseDirection = 1;
var humanEye1Node; var humanEye1Angle = 3.036872898470133; var humanEye1Direction = 1;
var humanEye2Node; var humanEye2Angle = -3.2463124087094526; var humanEye2Direction = 1;
var humanBodyNode;
var humanLeg1Node; var humanLeg1Angle = 2.792526803190927; var humanLeg1Direction = 1;
var humanLeg2Node; var humanLeg2Angle = 2.792526803190927; var humanLeg2Direction = 1;
var humanArm1Node; var humanArm1Angle = 2.792526803190927; var humanArm1Direction = 1;
var humanArm2Node; var humanArm2Angle = 2.792526803190927; var humanArm2Direction = 1;

var baseAntNode; var baseAntAngle = 0;
var firstLegNode; var firstLegAngle = 0; var firstLegDirection = 1;
var secondLegNode; var secondLegAngle = Math.PI / 4; var secondLegDirection = 1;
var thirdLegNode; var thirdLegAngle = 0; var thirdLegDirection = 1;
var fourthLegNode; var fourthLegAngle = -Math.PI / 4; var fourthLegDirection = 1;
var fifthLegNode; var fifthLegAngle = 0; var fifthLegDirection = 1;
var sixthLegNode; var sixthLegAngle = -Math.PI / 4; var sixthLegDirection = 1;
var headNode; var headAngle = 0; var headDirection = 1;
var firstAnthenaNode; var firstAnthenaAngle = Math.PI / 24; var firstAnthenaDirection = 1;
var secondAnthenaNode; var secondAnthenaAngle = -Math.PI / 24; var secondAnthenaDirection = 1;
var stomachNode; var stomachAngle = 0; var stomachDirection = 1;
var firstChildLegNode; var firstChildLegAngle = 0; var firstChildLegDirection = 1;
var secondChildLegNode; var secondChildLegAngle = 0; var secondChildLegDirection = 1;
var thirdChildLegNode; var thirdChildLegAngle = 0; var thirdChildLegDirection = 1;
var fourthChildLegNode; var fourthChildLegAngle = 0; var fourthChildLegDirection = 1;
var fifthChildLegNode; var fifthChildLegAngle = 0; var fifthChildLegDirection = 1;
var sixthChildLegNode; var sixthChildLegAngle = 0; var sixthChildLegDirection = 1;

var baseDeskNode; var baseDeskAngle = 0;
var firstDeskLegNode; var firstDeskLegAngle = 0;
var secondDeskLegNode; var secondDeskLegAngle = 0;
var thirdDeskLegNode; var thirdDeskLegAngle = 0;
var fourthDeskLegNode; var fourthDeskLegAngle = 0;

var baseChairNode; var baseChairAngle = 0;
var backChairNode; var backChairAngle = 0;
var firstChairLegNode; var firstChairLegAngle = 0;
var secondChairLegNode; var secondChairLegAngle = 0;
var thirdChairLegNode; var thirdChairLegAngle = 0;
var fourthChairLegNode; var fourthChairLegAngle = 0;


var mode = "shading";
var cameraMode = true
var drawType;


function drawLightSource(shadow) {
    mvPushMatrix();
    //item specific modifications
    //draw
    setupToDrawSphere(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(1, shadow);
    setupMaterial("bronze", shadow);
    gl.drawElements(gl.TRIANGLES, sphereVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawRoom(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [10.0, 5.0, 30.0]);
    //draw
    setupToDrawCubeInsides(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(1, shadow);
    setupMaterial(roomMaterial, shadow);
    gl.drawElements(gl.TRIANGLES, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}


function drawDogBase(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.7, 0.8, 2.0]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(8, shadow);
    setupMaterial(dogMaterial, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawneck(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.3, 0.8, 0.2]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(8, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawhead(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.4, 0.3, 0.7]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(8, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawEars(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.1, 0.4, 0.1]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(11, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawEyes(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.04, 0.3, 0.2]);
    //draw
    setupToDrawCylinder(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(9, shadow);
    gl.drawElements(drawType, cylinderVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawTailBase(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.15, 0.4, 0.15]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(10, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawTailTop(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.1, 0.5, 0.1]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(8, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawLegBase(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.15, 1, 0.15]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(10, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawHumanBase(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.5, 0.5, 0.8]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(12, shadow);
    setupMaterial(humanMaterial, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawHumanNose(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.1, 0.4, 0.1]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(7, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawHumanEye(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [-0.1, -0.1, 0.2]);
    //draw
    setupToDrawCylinder(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(9, shadow);
    gl.drawElements(drawType, cylinderVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawHumanBody(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.5, 1.5, 0.5]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(12, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawHumanLeg(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.15, 1, 0.15]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(7, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawHumanArm(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.15, 1, 0.15]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(7, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

// ANT

function drawAntBase(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [1.0, 0.5, 0.75]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(6, shadow);
    setupMaterial(antMaterial, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawAntLeg(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.15, 0.15, 0.7]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(6, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawChildLeg(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.15, 0.7, 0.15]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(6, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawAntHead(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.7, 0.4, 0.5]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(6, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawAnthenaHead(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.1, 1.0, 0.1]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(6, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawStomach(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.7, 0.7, 0.7]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(6, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

// DESK
function drawDeskBase(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [2.0, 0.2, 2.0]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(12, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawDeskLeg(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.2, 1.0, 0.2]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(12, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

// Chair
function drawChairBase(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [1.0, 0.2, 1.0]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(12, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawChairBack(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.2, 1.0, 1.0]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(12, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}

function drawChairLeg(shadow) {
    mvPushMatrix();
    //item specific modifications
    mat4.scale(mvMatrix, [0.2, 1.0, 0.2]);
    //draw
    setupToDrawCube(shadow);
    setMatrixUniforms(shadow);
    chooseTexture(12, shadow);
    gl.drawElements(drawType, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
    mvPopMatrix(shadow);
}


function initObjectTree() {
    if (mode == "shading") {
        drawType = gl.TRIANGLES;
    }
    else {
        drawType = gl.LINES;
    }
    lightSourceNode = { "draw": drawLightSource, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(lightSourceNode.matrix, [document.getElementById("lightPositionX").value / 10.0, document.getElementById("lightPositionY").value / 10.0, document.getElementById("lightPositionZ").value / 10.0]);

    roomNode = { "draw": drawRoom, "matrix": mat4.identity(mat4.create()) };

    //Dog

    baseDogNode = { "draw": drawDogBase, "matrix": mat4.identity(mat4.create()), "location": [-6.0, -2.0, 0.0], "rotation": baseDogAngle };
    mat4.translate(baseDogNode.matrix, [-6.0, -2.0, 0.0]);
    mat4.rotate(baseDogNode.matrix, baseDogAngle, [0.0, 1.0, 0.0]);



    neckNode = { "draw": drawneck, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(neckNode.matrix, [0.0, -0.4, 1.4]);
    mat4.rotate(neckNode.matrix, neckAngle, [1.0, 0.0, 0.0]);
    mat4.translate(neckNode.matrix, [0.0, 2.0, 0.0]);

    headDogNode = { "draw": drawhead, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(headDogNode.matrix, [0.0, 1.0, 0.4]);
    mat4.rotate(headDogNode.matrix, headDogAngle, [0.0, 1.0, 0.0]);


    eye1Node = { "draw": drawEyes, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(eye1Node.matrix, [eye1Translation, 0.2, 0.4]); //0.25
    mat4.rotate(eye1Node.matrix, Math.PI / 2, [0, 0, 1]);


    eye2Node = { "draw": drawEyes, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(eye2Node.matrix, [eye2Translation, 0.2, 0.4]); //0.25
    mat4.rotate(eye2Node.matrix, Math.PI / 2, [0.0, 0.0, -1.0]);


    firstEarsNode = { "draw": drawEars, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(firstEarsNode.matrix, [0.45, 0.25, -0.45]);
    mat4.rotate(firstEarsNode.matrix, firstEarsAngle, [1.0, 0.0, 0.0]);
    mat4.translate(firstEarsNode.matrix, [0.1, 0.1, 0.0]);


    secondEarsNode = { "draw": drawEars, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(secondEarsNode.matrix, [-0.45, 0.25, -0.45]);
    mat4.rotate(secondEarsNode.matrix, secondEarsAngle, [1.0, 0.0, 0.0]);
    mat4.translate(secondEarsNode.matrix, [0.1, 0.1, 0.0]);



    tailBaseNode = { "draw": drawTailBase, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(tailBaseNode.matrix, [0.0, 0.65, -2]);
    mat4.rotate(tailBaseNode.matrix, tailBaseAngle, [1.0, 0.0, 0.0]);
    mat4.translate(tailBaseNode.matrix, [0.0, 0.4, 0.0]);


    tailTopNode = { "draw": drawTailTop, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(tailTopNode.matrix, [0.0, 0.3, 0.0]);
    mat4.rotate(tailTopNode.matrix, tailTopAngle, [1.0, 0.0, 0.0]);
    mat4.translate(tailTopNode.matrix, [0.0, 0.3, 0.0]);

    firstLegBaseNode = { "draw": drawLegBase, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(firstLegBaseNode.matrix, [0.45, -0.5, 1.5]);
    mat4.rotate(firstLegBaseNode.matrix, firstLegBaseAngle, [0.1, 0.0, 0.0]);
    mat4.translate(firstLegBaseNode.matrix, [0.0, 1, 0.0]);

    secondLegBaseNode = { "draw": drawLegBase, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(secondLegBaseNode.matrix, [-0.45, -0.5, 1.5]);
    mat4.rotate(secondLegBaseNode.matrix, secondLegBaseAngle, [-0.1, 0.0, 0.0]);
    mat4.translate(secondLegBaseNode.matrix, [0.0, 1, 0.0]);

    thirdLegBaseNode = { "draw": drawLegBase, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(thirdLegBaseNode.matrix, [0.45, -0.5, -1.5]);
    mat4.rotate(thirdLegBaseNode.matrix, thirdLegBaseAngle, [-0.1, 0.0, 0.0]);
    mat4.translate(thirdLegBaseNode.matrix, [0.0, -1, 0.0]);

    fourthLegBaseNode = { "draw": drawLegBase, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(fourthLegBaseNode.matrix, [-0.45, -0.5, -1.5]);
    mat4.rotate(fourthLegBaseNode.matrix, fourthLegBaseAngle, [0.1, 0.0, 0.0]);
    mat4.translate(fourthLegBaseNode.matrix, [0.0, -1, 0.0]);


    //Human
    humanBodyNode = { "draw": drawHumanBody, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(humanBodyNode.matrix, [6.0, -2, 4.0]);


    baseHumanNode = { "draw": drawHumanBase, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(baseHumanNode.matrix, [0.0, 2.0, 0.0]);
    mat4.rotate(baseHumanNode.matrix, baseHumanAngle, [0.0, 1.0, 0.0]);

    humanNoseNode = { "draw": drawHumanNose, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(humanNoseNode.matrix, [humanNoseTranslation, 0.0, 0.0]);
    mat4.rotate(humanNoseNode.matrix, Math.PI / 2, [0.0, 0.0, 1.0]);



    humanEye1Node = { "draw": drawHumanEye, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(humanEye1Node.matrix, [0.5, 0.2, 0.4]);
    mat4.rotate(humanEye1Node.matrix, humanEye1Angle, [1, 0, 0.0]);
    mat4.translate(humanEye1Node.matrix, [0.0, 0.0, 0.0]);

    humanEye2Node = { "draw": drawHumanEye, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(humanEye2Node.matrix, [0.5, 0.2, -0.4]);
    mat4.rotate(humanEye2Node.matrix, humanEye2Angle, [-1, 0, 0.0]);
    mat4.translate(humanEye2Node.matrix, [0.0, 0.0, 0.0]);

    humanLeg1Node = { "draw": drawHumanLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(humanLeg1Node.matrix, [0.3, -1, 0]);
    mat4.rotate(humanLeg1Node.matrix, humanLeg1Angle, [0.1, 0.0, 0.0]);
    mat4.translate(humanLeg1Node.matrix, [0.0, 1, 0.0]);

    humanLeg2Node = { "draw": drawHumanLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(humanLeg2Node.matrix, [-0.3, -1, 0]);
    mat4.rotate(humanLeg2Node.matrix, humanLeg2Angle, [-0.1, 0.0, 0.0]);
    mat4.translate(humanLeg2Node.matrix, [0.0, 1, 0.0]);

    humanArm1Node = { "draw": drawHumanArm, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(humanArm1Node.matrix, [0.6, 0.9, 0]);
    mat4.rotate(humanArm1Node.matrix, humanArm1Angle, [0.1, 0.0, 0.0]);
    mat4.translate(humanArm1Node.matrix, [0.0, 1, 0.0]);

    humanArm2Node = { "draw": drawHumanArm, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(humanArm2Node.matrix, [-0.6, 0.9, 0]);
    mat4.rotate(humanArm2Node.matrix, humanArm2Angle, [-0.1, 0.0, 0.0]);
    mat4.translate(humanArm2Node.matrix, [0.0, 1, 0.0]);

    // ANT
    baseAntNode = { "draw": drawAntBase, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(baseAntNode.matrix, [-2.0, -3.0, 5.0]);
    mat4.rotate(baseAntNode.matrix, baseAntAngle, [0.0, 1.0, 0.0]);

    firstLegNode = { "draw": drawAntLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(firstLegNode.matrix, [-0.45, -0.25, 0.45]);
    mat4.rotate(firstLegNode.matrix, firstLegAngle, [-1.0, -0.15, 0.0]);
    mat4.translate(firstLegNode.matrix, [1.3, 0.0, 0.5]);

    secondLegNode = { "draw": drawAntLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(secondLegNode.matrix, [-0.45, -0.25, 0.45]);
    mat4.rotate(secondLegNode.matrix, secondLegAngle, [-1.0, -0.15, 0.0]);
    mat4.translate(secondLegNode.matrix, [0.7, 0.0, 0.5]);

    thirdLegNode = { "draw": drawAntLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(thirdLegNode.matrix, [-0.45, -0.25, 0.45]);
    mat4.rotate(thirdLegNode.matrix, thirdLegAngle, [-1.0, -0.15, 0.0]);
    mat4.translate(thirdLegNode.matrix, [0.1, 0.0, 0.5]);

    fourthLegNode = { "draw": drawAntLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(fourthLegNode.matrix, [-0.45, -0.25, 0.45]);
    mat4.rotate(fourthLegNode.matrix, fourthLegAngle, [-1.0, -0.15, 0.0]);
    mat4.translate(fourthLegNode.matrix, [0.1, -0.0, -1.3]);

    fifthLegNode = { "draw": drawAntLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(fifthLegNode.matrix, [-0.45, -0.25, 0.45]);
    mat4.rotate(fifthLegNode.matrix, fifthLegAngle, [-1.0, -0.15, 0.0]);
    mat4.translate(fifthLegNode.matrix, [0.7, -0.0, -1.3]);

    sixthLegNode = { "draw": drawAntLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(sixthLegNode.matrix, [-0.45, -0.25, 0.45]);
    mat4.rotate(sixthLegNode.matrix, sixthLegAngle, [-1.0, -0.15, 0.0]);
    mat4.translate(sixthLegNode.matrix, [1.3, -0.0, -1.3]);

    headNode = { "draw": drawAntHead, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(headNode.matrix, [-0.45, -0.25, 0.45]);
    mat4.rotate(headNode.matrix, headAngle, [0.0, 0.0, 0.15]);
    mat4.translate(headNode.matrix, [1.8, 0.3, -0.5]);

    firstAnthenaNode = { "draw": drawAnthenaHead, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(firstAnthenaNode.matrix, [-0.45, -0.25, 0.45]);
    mat4.rotate(firstAnthenaNode.matrix, firstAnthenaAngle, [0.0, 0.0, 0.15]);
    mat4.translate(firstAnthenaNode.matrix, [0.7, 1.0, -0.2]);

    secondAnthenaNode = { "draw": drawAnthenaHead, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(secondAnthenaNode.matrix, [-0.45, -0.25, 0.45]);
    mat4.rotate(secondAnthenaNode.matrix, secondAnthenaAngle, [0.0, 0.0, 0.15]);
    mat4.translate(secondAnthenaNode.matrix, [0.7, 1.0, -0.8]);

    stomachNode = { "draw": drawStomach, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(stomachNode.matrix, [-0.45, -0.25, 0.45]);
    mat4.rotate(stomachNode.matrix, stomachAngle, [0.0, 0.0, 0.15]);
    mat4.translate(stomachNode.matrix, [-1.2, 0.6, -0.4]);

    firstChildLegNode = { "draw": drawChildLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(firstChildLegNode.matrix, [0.0, 0.0, 0.0]);
    mat4.rotate(firstChildLegNode.matrix, firstChildLegAngle, [0.0, 0.0, 0.0]);
    mat4.translate(firstChildLegNode.matrix, [0.0, -0.55, 0.7]);

    secondChildLegNode = { "draw": drawChildLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(secondChildLegNode.matrix, [0.0, 0.0, 0.0]);
    mat4.rotate(secondChildLegNode.matrix, secondChildLegAngle, [0.0, 0.0, 0.0]);
    mat4.translate(secondChildLegNode.matrix, [0.0, -0.55, 0.7]);

    thirdChildLegNode = { "draw": drawChildLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(thirdChildLegNode.matrix, [0.0, 0.0, 0.0]);
    mat4.rotate(thirdChildLegNode.matrix, thirdChildLegAngle, [0.0, 0.0, 0.0]);
    mat4.translate(thirdChildLegNode.matrix, [0.0, -0.55, 0.7]);

    fourthChildLegNode = { "draw": drawChildLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(fourthChildLegNode.matrix, [0.0, 0.0, 0.0]);
    mat4.rotate(fourthChildLegNode.matrix, fourthChildLegAngle, [0.0, 0.0, 0.0]);
    mat4.translate(fourthChildLegNode.matrix, [0.0, -0.55, -0.7]);

    fifthChildLegNode = { "draw": drawChildLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(fifthChildLegNode.matrix, [0.0, 0.0, 0.0]);
    mat4.rotate(fifthChildLegNode.matrix, fifthChildLegAngle, [0.0, 0.0, 0.0]);
    mat4.translate(fifthChildLegNode.matrix, [0.0, -0.55, -0.7]);

    sixthChildLegNode = { "draw": drawChildLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(sixthChildLegNode.matrix, [0.0, 0.0, 0.0]);
    mat4.rotate(sixthChildLegNode.matrix, sixthChildLegAngle, [0.0, 0.0, 0.0]);
    mat4.translate(sixthChildLegNode.matrix, [0.0, -0.55, -0.7]);

    // DESK
    baseDeskNode = { "draw": drawDeskBase, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(baseDeskNode.matrix, [2.0, -3.0, -5.0]);
    mat4.rotate(baseDeskNode.matrix, baseDeskAngle, [0.0, 1.0, 0.0]);

    firstDeskLegNode = { "draw": drawDeskLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(firstDeskLegNode.matrix, [0.9, -0.9, 0.9]);
    mat4.rotate(firstDeskLegNode.matrix, firstDeskLegAngle, [0.0, 1.0, 0.0]);

    secondDeskLegNode = { "draw": drawDeskLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(secondDeskLegNode.matrix, [-0.9, -0.9, 0.9]);
    mat4.rotate(secondDeskLegNode.matrix, secondDeskLegAngle, [0.0, 1.0, 0.0]);

    thirdDeskLegNode = { "draw": drawDeskLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(thirdDeskLegNode.matrix, [0.9, -0.9, -0.9]);
    mat4.rotate(thirdDeskLegNode.matrix, thirdDeskLegAngle, [0.0, 1.0, 0.0]);

    fourthDeskLegNode = { "draw": drawDeskLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(fourthDeskLegNode.matrix, [-0.9, -0.9, -0.9]);
    mat4.rotate(fourthDeskLegNode.matrix, fourthDeskLegAngle, [0.0, 1.0, 0.0]);

    // CHAIR
    baseChairNode = { "draw": drawChairBase, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(baseChairNode.matrix, [7.0, -3.0, -5.0]);
    mat4.rotate(baseChairNode.matrix, baseChairAngle, [0.0, 1.0, 0.0]);

    backChairNode = { "draw": drawChairBack, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(backChairNode.matrix, [1.0, 1.0, 0.0]);
    mat4.rotate(backChairNode.matrix, backChairAngle, [0.0, 1.0, 0.0]);

    firstChairLegNode = { "draw": drawChairLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(firstChairLegNode.matrix, [0.8, -0.8, 0.8]);
    mat4.rotate(firstChairLegNode.matrix, firstChairLegAngle, [0.0, 1.0, 0.0]);

    secondChairLegNode = { "draw": drawChairLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(secondChairLegNode.matrix, [-0.8, -0.8, 0.8]);
    mat4.rotate(secondChairLegNode.matrix, secondChairLegAngle, [0.0, 1.0, 0.0]);

    thirdChairLegNode = { "draw": drawChairLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(thirdChairLegNode.matrix, [0.8, -0.8, -0.8]);
    mat4.rotate(thirdChairLegNode.matrix, thirdChairLegAngle, [0.0, 1.0, 0.0]);

    fourthChairLegNode = { "draw": drawChairLeg, "matrix": mat4.identity(mat4.create()) };
    mat4.translate(fourthChairLegNode.matrix, [-0.8, -0.8, -0.8]);
    mat4.rotate(fourthChairLegNode.matrix, fourthChairLegAngle, [0.0, 1.0, 0.0]);


    baseDogNode.child = neckNode;
    neckNode.child = headDogNode;
    headDogNode.child = eye1Node;
    eye1Node.sibling = eye2Node;
    eye2Node.sibling = firstEarsNode;
    firstEarsNode.sibling = secondEarsNode;
    neckNode.sibling = tailBaseNode;
    tailBaseNode.child = tailTopNode;
    tailBaseNode.sibling = firstLegBaseNode;
    firstLegBaseNode.sibling = secondLegBaseNode;
    secondLegBaseNode.sibling = thirdLegBaseNode;
    thirdLegBaseNode.sibling = fourthLegBaseNode;

    baseDogNode.sibling = humanBodyNode;
    humanBodyNode.child = baseHumanNode;
    baseHumanNode.child = humanNoseNode;
    humanNoseNode.sibling = humanEye1Node;
    humanEye1Node.sibling = humanEye2Node;
    baseHumanNode.sibling = humanLeg1Node;
    humanLeg1Node.sibling = humanLeg2Node;
    humanLeg2Node.sibling = humanArm1Node;
    humanArm1Node.sibling = humanArm2Node;

    humanBodyNode.sibling = baseAntNode;
    baseAntNode.child = firstLegNode;
    firstLegNode.sibling = secondLegNode;
    secondLegNode.sibling = thirdLegNode;
    thirdLegNode.sibling = fourthLegNode;
    fourthLegNode.sibling = fifthLegNode;
    fifthLegNode.sibling = sixthLegNode;
    sixthLegNode.sibling = headNode;
    headNode.child = firstAnthenaNode;
    firstAnthenaNode.sibling = secondAnthenaNode;
    headNode.sibling = stomachNode;
    firstLegNode.child = firstChildLegNode;
    secondLegNode.child = secondChildLegNode;
    thirdLegNode.child = thirdChildLegNode;
    fourthLegNode.child = fourthChildLegNode;
    fifthLegNode.child = fifthChildLegNode;
    sixthLegNode.child = sixthChildLegNode;

    baseAntNode.sibling = baseDeskNode;
    baseDeskNode.child = firstDeskLegNode;
    firstDeskLegNode.sibling = secondDeskLegNode;
    secondDeskLegNode.sibling = thirdDeskLegNode;
    thirdDeskLegNode.sibling = fourthDeskLegNode;

    baseDeskNode.sibling = baseChairNode;
    baseChairNode.child = backChairNode;
    backChairNode.sibling = firstChairLegNode;
    firstChairLegNode.sibling = secondChairLegNode;
    secondChairLegNode.sibling = thirdChairLegNode;
    thirdChairLegNode.sibling = fourthChairLegNode;
}

function traverse(node, shadow) {
    mvPushMatrix();
    //modifications
    mat4.multiply(mvMatrix, node.matrix);
    //draw
    node.draw(shadow);
    if ("child" in node) traverse(node.child, shadow);
    mvPopMatrix(shadow);
    if ("sibling" in node) traverse(node.sibling, shadow);
}

var shadowMapLookAtMatrix = mat4.create();
var shadowMapPerspectiveMatrix = mat4.create();
var shadowMapTransform = mat4.create();

//draws shadowmap for the side of the texture
//0: positive x, ..., 5: negative z
function drawShadowMap(side) {
    var centers = [
        1.0, 0.0, 0.0, //positive x
        -1.0, 0.0, 0.0, //negative x
        0.0, 1.0, 0.0, //positive y
        0.0, -1.0, 0.0, //negative y
        0.0, 0.0, 1.0, //positive z
        0.0, 0.0, -1.0, //negative z
    ];

    var upVectors = [
        0.0, -1.0, 0.0, //positive x
        0.0, -1.0, 0.0, //negative x
        0.0, 0.0, 1.0, //positive y
        0.0, 0.0, -1.0, //negative y
        0.0, -1.0, 0.0, //positive z
        0.0, -1.0, 0.0, //negative z
    ];
    gl.useProgram(shadowMapShaderProgram);
    gl.bindFramebuffer(gl.FRAMEBUFFER, shadowFrameBuffer);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_CUBE_MAP_POSITIVE_X + side, shadowFrameBuffer.depthBuffer, 0);

    gl.viewport(0, 0, shadowFrameBuffer.width, shadowFrameBuffer.height);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    shadowMapLookAtMatrix = mat4.create();
    lookAt(shadowMapLookAtMatrix,
        parseFloat(document.getElementById("lightPositionX").value / 10.0),
        parseFloat(document.getElementById("lightPositionY").value / 10.0),
        parseFloat(document.getElementById("lightPositionZ").value / 10.0),
        parseFloat(document.getElementById("lightPositionX").value / 10.0) + centers[side * 3],
        parseFloat(document.getElementById("lightPositionY").value / 10.0) + centers[side * 3 + 1],
        parseFloat(document.getElementById("lightPositionZ").value / 10.0) + centers[side * 3 + 2],
        upVectors[side * 3],
        upVectors[side * 3 + 1],
        upVectors[side * 3 + 2]);
    mat4.perspective(90, shadowFrameBuffer.width / shadowFrameBuffer.height, 0.1, 100.0, shadowMapTransform);
    mat4.multiply(shadowMapTransform, shadowMapLookAtMatrix);
    mat4.set(shadowMapTransform, pMatrix);

    gl.uniform3f(
        shadowMapShaderProgram.pointLightingLocationUniform,
        parseFloat(document.getElementById("lightPositionX").value / 10.0),
        parseFloat(document.getElementById("lightPositionY").value / 10.0),
        parseFloat(document.getElementById("lightPositionZ").value / 10.0)
    );
    gl.uniform1f(shadowMapShaderProgram.uFarPlaneUniform, 100.0);

    mat4.identity(mvMatrix);
    traverse(roomNode, true);
    mat4.translate(mvMatrix, [0, 0, -20]);
    traverse(baseDogNode, true);

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
}

var lookAtMatrix;
function drawScene() {
    lookAtMatrix = mat4.create();
    gl.useProgram(shaderProgram);
    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    pMatrix = mat4.create();
    if (cameraMode) {
        lookAt(lookAtMatrix,
            parseFloat(document.getElementById("eyePosition1").value / 10.0),
            parseFloat(document.getElementById("eyePosition2").value / 10.0),
            parseFloat(document.getElementById("eyePosition3").value / 10.0),
            parseFloat(document.getElementById("centerPosition1").value / 10.0),
            parseFloat(document.getElementById("centerPosition2").value / 10.0),
            parseFloat(document.getElementById("centerPosition3").value / 10.0),
            parseFloat(document.getElementById("upPosition1").value / 10.0),
            parseFloat(document.getElementById("upPosition2").value / 10.0),
            parseFloat(document.getElementById("upPosition3").value / 10.0));
        mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);
        mat4.multiply(pMatrix, lookAtMatrix);
    }
    else {

        lookAt(lookAtMatrix,
            0.0, 0.0, 5.0,
            0.0, 0.0, 6.0,
            0, 1, 0.0);
        mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);
        var newMat = mat4.identity(mat4.create())
        baseDogNode.location[2] -= 20
        mat4.translate(newMat, baseDogNode.location);
        mat4.rotate(newMat, baseDogNode.rotation, [0.0, 1.0, 0.0]);
        mat4.multiply(pMatrix, lookAtMatrix);
        mat4.multiply(pMatrix, mat4.inverse(newMat, mat4.create()));
    }






    gl.uniform1i(shaderProgram.useLightingUniform, document.getElementById("lighting").checked);
    gl.uniform1i(shaderProgram.useTextureUniform, document.getElementById("texture").checked);

    gl.uniform3f(
        shaderProgram.ambientColorUniform,
        parseFloat(document.getElementById("ambientR").value),
        parseFloat(document.getElementById("ambientG").value),
        parseFloat(document.getElementById("ambientB").value)
    );
    gl.uniform3f(
        shaderProgram.pointLightingLocationUniform,
        parseFloat(document.getElementById("lightPositionX").value / 10.0),
        parseFloat(document.getElementById("lightPositionY").value / 10.0),
        parseFloat(document.getElementById("lightPositionZ").value / 10.0)
    );
    gl.uniform3f(
        shaderProgram.pointLightingDiffuseColorUniform,
        parseFloat(document.getElementById("pointR").value),
        parseFloat(document.getElementById("pointG").value),
        parseFloat(document.getElementById("pointB").value)
    );
    gl.uniform3f(
        shaderProgram.pointLightingSpecularColorUniform,
        parseFloat(document.getElementById("pointR").value),
        parseFloat(document.getElementById("pointG").value),
        parseFloat(document.getElementById("pointB").value)
    );

    gl.activeTexture(gl.TEXTURE31);
    gl.bindTexture(gl.TEXTURE_CUBE_MAP, shadowFrameBuffer.depthBuffer);
    gl.uniform1i(shaderProgram.shadowMapUniform, 31);

    gl.uniform1f(shaderProgram.uFarPlaneUniform, 100.0);

    mat4.identity(mvMatrix);
    traverse(lightSourceNode, false);
    traverse(roomNode, false);

    mat4.translate(mvMatrix, [0, 0, -20]);
    traverse(baseDogNode, false);

}

function animate() {
    if (animating) {
        //var update = (0.05 * Math.PI * (timeNow - lastTime)/ 180); //use elapsed time, which is faulty on changing tabs
        var update = (0.05 * Math.PI * 10 / 180);

        //ARM
        baseDogAngle = (baseDogAngle + update) % (2 * Math.PI);
        document.getElementById("baseDogRotationSlider").value = baseDogAngle * 180 / (Math.PI);

        neckAngle += update * neckDirection;
        if (neckAngle < 0 && neckDirection == -0.1) neckDirection *= -1;
        if (neckAngle > Math.PI / 12 && neckDirection == 0.1) neckDirection *= -1;
        document.getElementById("neckRotationSlider").value = neckAngle * 180 / (Math.PI);

        headDogAngle += update * headDogDirection;
        if (headDogAngle < -Math.PI / 4 && headDogDirection == -1) headDogDirection *= -1;
        if (headDogAngle > Math.PI / 4 && headDogDirection == 1) headDogDirection *= -1;
        document.getElementById("headDogRotationSlider").value = headDogAngle * 180 / (Math.PI);

        eye1Translation += 0.5 * update * eye1Direction;
        if (eye1Translation < 0.5 && eye1Direction == -1) eye1Direction *= -1;
        if (eye1Translation > 0.6 && eye1Direction == 1) eye1Direction *= -1;
        document.getElementById("eye1TranslationSlider").value = eye1Translation * 100;

        eye2Translation += 0.5 * update * eye2Direction;
        if (eye2Translation < -0.6 && eye2Direction == -1) eye2Direction *= -1;
        if (eye2Translation > -0.5 && eye2Direction == 1) eye2Direction *= -1;
        document.getElementById("eye2TranslationSlider").value = eye2Translation * 100;

        firstEarsAngle += update * firstEarsDirection;
        if (firstEarsAngle < -Math.PI / 4 && firstEarsDirection == -1) firstEarsDirection *= -1;
        if (firstEarsAngle > Math.PI / 8 && firstEarsDirection == 1) firstEarsDirection *= -1;
        document.getElementById("firstEarsRotationSlider").value = firstEarsAngle * 180 / (Math.PI);

        secondEarsAngle += update * secondEarsDirection;
        if (secondEarsAngle < -Math.PI / 4 && secondEarsDirection == -1) secondEarsDirection *= -1;
        if (secondEarsAngle > Math.PI / 8 && secondEarsDirection == 1) secondEarsDirection *= -1;
        document.getElementById("secondEarsRotationSlider").value = secondEarsAngle * 180 / (Math.PI);

        firstLegBaseAngle += update * firstLegBaseDirection;
        if (firstLegBaseAngle < 0.89 * Math.PI && firstLegBaseDirection == -1) firstLegBaseDirection *= -1;
        if (firstLegBaseAngle > 1.23 * Math.PI && firstLegBaseDirection == 1) firstLegBaseDirection *= -1;
        document.getElementById("firstLegBaseRotationSlider").value = firstLegBaseAngle * 180 / (Math.PI);

        secondLegBaseAngle += update * secondLegBaseDirection;
        if (secondLegBaseAngle < 0.89 * Math.PI && secondLegBaseDirection == -1) secondLegBaseDirection *= -1;
        if (secondLegBaseAngle > 1.23 * Math.PI && secondLegBaseDirection == 1) secondLegBaseDirection *= -1;
        document.getElementById("secondLegBaseRotationSlider").value = secondLegBaseAngle * 180 / (Math.PI);

        thirdLegBaseAngle += update * thirdLegBaseDirection;
        if (thirdLegBaseAngle < -0.138 * Math.PI && thirdLegBaseDirection == -1) thirdLegBaseDirection *= -1;
        if (thirdLegBaseAngle > 0.195 * Math.PI && thirdLegBaseDirection == 1) thirdLegBaseDirection *= -1;
        document.getElementById("thirdLegBaseRotationSlider").value = thirdLegBaseAngle * 180 / (Math.PI);

        // console.log(thirdLegBaseAngle);

        fourthLegBaseAngle += update * fourthLegBaseDirection;
        if (fourthLegBaseAngle < -0.138 * Math.PI && fourthLegBaseDirection == -1) fourthLegBaseDirection *= -1;
        if (fourthLegBaseAngle > 0.195 * Math.PI && fourthLegBaseDirection == 1) fourthLegBaseDirection *= -1;
        document.getElementById("fourthLegBaseRotationSlider").value = fourthLegBaseAngle * 180 / (Math.PI);

        tailBaseAngle += update * tailBaseDirection;
        if (tailBaseAngle < -3 * Math.PI / 4 && tailBaseDirection == -1) tailBaseDirection *= -1;
        if (tailBaseAngle > -Math.PI / 2 && tailBaseDirection == 1) tailBaseDirection *= -1;
        document.getElementById("tailBaseRotationSlider").value = tailBaseAngle * 180 / (Math.PI);


        //    console.log(tailBaseAngle)

        tailTopAngle += update * tailTopDirection;
        if (tailTopAngle < 0 && tailTopDirection == -1) tailTopDirection *= -1;
        if (tailTopAngle > Math.PI / 8 && tailTopDirection == 1) tailTopDirection *= -1;
        document.getElementById("tailTopRotationSlider").value = tailTopAngle * 180 / (Math.PI);

        //model 2
        baseHumanAngle += update * baseHumanDirection;
        if (baseHumanAngle < 1.17 * Math.PI && baseHumanDirection == -1) baseHumanDirection *= -1;
        if (baseHumanAngle > 1.84 * Math.PI && baseHumanDirection == 1) baseHumanDirection *= -1;
        document.getElementById("baseHumanRotationSlider").value = baseHumanAngle * 180 / (Math.PI);


        humanNoseTranslation += 1 * update * humanNoseDirection;
        if (humanNoseTranslation < 0.2 && humanNoseDirection == -1) humanNoseDirection *= -1;
        if (humanNoseTranslation > 0.8 && humanNoseDirection == 1) humanNoseDirection *= -1;
        document.getElementById("noseTranslationSlider").value = humanNoseTranslation * 180 / (Math.PI);

        humanEye1Angle += update * humanEye1Direction;
        if (humanEye1Angle < 0.84 * Math.PI && humanEye1Direction == -1) humanEye1Direction *= -1;
        if (humanEye1Angle > 1.12 * Math.PI && humanEye1Direction == 1) humanEye1Direction *= -1;
        document.getElementById("humanEye1RotationSlider").value = humanEye1Angle * 180 / (Math.PI);

        humanEye2Angle += update * humanEye2Direction;
        if (humanEye2Angle < -1.16 * Math.PI && humanEye2Direction == -1) humanEye2Direction *= -1;
        if (humanEye2Angle > -0.88 * Math.PI && humanEye2Direction == 1) humanEye2Direction *= -1;
        document.getElementById("humanEye2RotationSlider").value = humanEye2Angle * 180 / (Math.PI);

        humanLeg1Angle += update * humanLeg1Direction;
        if (humanLeg1Angle < 0.89 * Math.PI && humanLeg1Direction == -1) humanLeg1Direction *= -1;
        if (humanLeg1Angle > 1.16 * Math.PI && humanLeg1Direction == 1) humanLeg1Direction *= -1;
        document.getElementById("humanLeg1RotationSlider").value = humanLeg1Angle * 180 / (Math.PI);


        humanLeg2Angle += update * humanLeg2Direction;
        if (humanLeg2Angle < 0.89 * Math.PI && humanLeg2Direction == -1) humanLeg2Direction *= -1;
        if (humanLeg2Angle > 1.16 * Math.PI && humanLeg2Direction == 1) humanLeg2Direction *= -1;
        document.getElementById("humanLeg2RotationSlider").value = humanLeg2Angle * 180 / (Math.PI);

        humanArm1Angle += update * humanArm1Direction;
        if (humanArm1Angle < 0.89 * Math.PI && humanArm1Direction == -1) humanArm1Direction *= -1;
        if (humanArm1Angle > 1.16 * Math.PI && humanArm1Direction == 1) humanArm1Direction *= -1;
        document.getElementById("humanArm1RotationSlider").value = humanArm1Angle * 180 / (Math.PI);


        humanArm2Angle += update * humanArm2Direction;
        if (humanArm2Angle < 0.89 * Math.PI && humanArm2Direction == -1) humanArm2Direction *= -1;
        if (humanArm2Angle > 1.16 * Math.PI && humanArm2Direction == 1) humanArm2Direction *= -1;
        document.getElementById("humanArm2RotationSlider").value = humanArm2Angle * 180 / (Math.PI);

        // ANT
        baseAntAngle = (baseAntAngle + update) % (2 * Math.PI);
        document.getElementById("baseAntRotationSlider").value = baseAntAngle * 180 / (Math.PI);

        firstLegAngle += update * firstLegDirection * 2;
        if (firstLegAngle < 0 && firstLegDirection == -1) firstLegDirection *= -1;
        if (firstLegAngle > Math.PI / 4 && firstLegDirection == 1) firstLegDirection *= -1;
        document.getElementById("firstLegRotationSlider").value = firstLegAngle * 180 / (Math.PI);

        secondLegAngle += update * secondLegDirection * 2;
        if (secondLegAngle < 0 && secondLegDirection == -1) secondLegDirection *= -1;
        if (secondLegAngle > Math.PI / 4 && secondLegDirection == 1) secondLegDirection *= -1;
        document.getElementById("secondLegRotationSlider").value = secondLegAngle * 180 / (Math.PI);

        thirdLegAngle += update * thirdLegDirection * 2;
        if (thirdLegAngle < 0 && thirdLegDirection == -1) thirdLegDirection *= -1;
        if (thirdLegAngle > Math.PI / 4 && thirdLegDirection == 1) thirdLegDirection *= -1;
        document.getElementById("thirdLegRotationSlider").value = thirdLegAngle * 180 / (Math.PI);

        fourthLegAngle += update * fourthLegDirection * 2;
        if (fourthLegAngle < -Math.PI / 4 && fourthLegDirection == -1) fourthLegDirection *= -1;
        if (fourthLegAngle > 0 && fourthLegDirection == 1) fourthLegDirection *= -1;
        document.getElementById("fourthLegRotationSlider").value = Math.abs(fourthLegAngle) * 180 / (Math.PI);
        // document.getElementById("fourthLegRotationSlider").value = 30;

        fifthLegAngle += update * fifthLegDirection * 2;
        if (fifthLegAngle < -Math.PI / 4 && fifthLegDirection == -1) fifthLegDirection *= -1;
        if (fifthLegAngle > 0 && fifthLegDirection == 1) fifthLegDirection *= -1;
        document.getElementById("fifthLegRotationSlider").value = Math.abs(fifthLegAngle) * 180 / (Math.PI);

        sixthLegAngle += update * sixthLegDirection * 2;
        if (sixthLegAngle < -Math.PI / 4 && sixthLegDirection == -1) sixthLegDirection *= -1;
        if (sixthLegAngle > 0 && sixthLegDirection == 1) sixthLegDirection *= -1;
        document.getElementById("sixthLegRotationSlider").value = Math.abs(sixthLegAngle) * 180 / (Math.PI);

        headAngle += update * headDirection;
        if (headAngle < -Math.PI / 24 && headDirection == -1) headDirection *= -1;
        if (headAngle > Math.PI / 48 && headDirection == 1) headDirection *= -1;
        document.getElementById("headRotationSlider").value = Math.abs(headAngle) * 180 / (Math.PI);

        firstAnthenaAngle += update * firstAnthenaDirection * 2;
        if (firstAnthenaAngle < -Math.PI / 24 && firstAnthenaDirection == -1) firstAnthenaDirection *= -1;
        if (firstAnthenaAngle > Math.PI / 24 && firstAnthenaDirection == 1) firstAnthenaDirection *= -1;
        document.getElementById("firstAnthenaRotationSlider").value = Math.abs(firstAnthenaAngle) * 180 / (Math.PI);

        secondAnthenaAngle += update * secondAnthenaDirection * 2;
        if (secondAnthenaAngle < -Math.PI / 24 && secondAnthenaDirection == -1) secondAnthenaDirection *= -1;
        if (secondAnthenaAngle > Math.PI / 24 && secondAnthenaDirection == 1) secondAnthenaDirection *= -1;
        document.getElementById("secondAnthenaRotationSlider").value = Math.abs(secondAnthenaAngle) * 180 / (Math.PI);

        stomachAngle += update * stomachDirection * 2;
        if (stomachAngle < -Math.PI / 24 && stomachDirection == -1) stomachDirection *= -1;
        if (stomachAngle > Math.PI / 24 && stomachDirection == 1) stomachDirection *= -1;
        document.getElementById("stomachRotationSlider").value = Math.abs(stomachAngle) * 180 / (Math.PI);

        // DESK
        baseDeskAngle = (baseDeskAngle + update) % (2 * Math.PI);

        // CHAIR
        baseChairAngle = (baseChairAngle + update) % (2 * Math.PI);

    }
    initObjectTree();
}

function tick() {
    requestAnimationFrame(tick);
    for (var i = 0; i < 6; i++) {
        drawShadowMap(i);
    }
    drawScene();
    animate();
}
function initInputs() {
    document.getElementById("animation").checked = true;
    document.getElementById("lighting").checked = true;
    document.getElementById("texture").checked = true;
    document.getElementById("animation").onchange = function () {
        animating ^= 1;
        if (animating) {
            document.getElementById("baseDogRotationSlider").disabled = true;
            document.getElementById("neckRotationSlider").disabled = true;
            document.getElementById("headDogRotationSlider").disabled = true;
            document.getElementById("eye1TranslationSlider").disabled = true;
            document.getElementById("eye2TranslationSlider").disabled = true;
            document.getElementById("firstEarsRotationSlider").disabled = true;
            document.getElementById("secondEarsRotationSlider").disabled = true;
            document.getElementById("firstLegBaseRotationSlider").disabled = true;
            document.getElementById("secondLegBaseRotationSlider").disabled = true;
            document.getElementById("thirdLegBaseRotationSlider").disabled = true;
            document.getElementById("fourthLegBaseRotationSlider").disabled = true;
            document.getElementById("tailBaseRotationSlider").disabled = true;
            document.getElementById("tailTopRotationSlider").disabled = true;
            document.getElementById("baseHumanRotationSlider").disabled = true;
            document.getElementById("noseTranslationSlider").disabled = true;
            document.getElementById("humanEye1RotationSlider").disabled = true;
            document.getElementById("humanEye2RotationSlider").disabled = true;
            document.getElementById("humanLeg1RotationSlider").disabled = true;
            document.getElementById("humanLeg2RotationSlider").disabled = true;
            document.getElementById("humanArm1RotationSlider").disabled = true;
            document.getElementById("humanArm2RotationSlider").disabled = true;
            document.getElementById("baseAntRotationSlider").disabled = true;
            document.getElementById("firstLegRotationSlider").disabled = true;
            document.getElementById("secondLegRotationSlider").disabled = true;
            document.getElementById("thirdLegRotationSlider").disabled = true;
            document.getElementById("fourthLegRotationSlider").disabled = true;
            document.getElementById("fifthLegRotationSlider").disabled = true;
            document.getElementById("sixthLegRotationSlider").disabled = true;
            document.getElementById("headRotationSlider").disabled = true;
            document.getElementById("firstAnthenaRotationSlider").disabled = true;
            document.getElementById("secondAnthenaRotationSlider").disabled = true;
            document.getElementById("stomachRotationSlider").disabled = true;
        } else {
            document.getElementById("baseDogRotationSlider").disabled = false;
            document.getElementById("neckRotationSlider").disabled = false;
            document.getElementById("headDogRotationSlider").disabled = false;
            document.getElementById("eye1TranslationSlider").disabled = false;
            document.getElementById("eye2TranslationSlider").disabled = false;
            document.getElementById("firstEarsRotationSlider").disabled = false;
            document.getElementById("secondEarsRotationSlider").disabled = false;
            document.getElementById("firstLegBaseRotationSlider").disabled = false;
            document.getElementById("secondLegBaseRotationSlider").disabled = false;
            document.getElementById("thirdLegBaseRotationSlider").disabled = false;
            document.getElementById("fourthLegBaseRotationSlider").disabled = false;
            document.getElementById("tailBaseRotationSlider").disabled = false;
            document.getElementById("tailTopRotationSlider").disabled = false;
            document.getElementById("baseHumanRotationSlider").disabled = false;
            document.getElementById("noseTranslationSlider").disabled = false;
            document.getElementById("humanEye1RotationSlider").disabled = false;
            document.getElementById("humanEye2RotationSlider").disabled = false;
            document.getElementById("humanArm1RotationSlider").disabled = false;
            document.getElementById("humanArm2RotationSlider").disabled = false;
            document.getElementById("baseAntRotationSlider").disabled = false;
            document.getElementById("firstLegRotationSlider").disabled = false;
            document.getElementById("secondLegRotationSlider").disabled = false;
            document.getElementById("thirdLegRotationSlider").disabled = false;
            document.getElementById("fourthLegRotationSlider").disabled = false;
            document.getElementById("fifthLegRotationSlider").disabled = false;
            document.getElementById("sixthLegRotationSlider").disabled = false;
            document.getElementById("headRotationSlider").disabled = false;
            document.getElementById("firstAnthenaRotationSlider").disabled = false;
            document.getElementById("secondAnthenaRotationSlider").disabled = false;
            document.getElementById("stomachRotationSlider").disabled = false;
        }
    };
    document.getElementById("baseDogRotationSlider").oninput = function () {
        baseDogAngle = document.getElementById("baseDogRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("neckRotationSlider").oninput = function () {
        neckAngle = document.getElementById("neckRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("headDogRotationSlider").oninput = function () {
        headDogAngle = document.getElementById("headDogRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("eye1TranslationSlider").oninput = function () {
        eye1Translation = document.getElementById("eye1TranslationSlider").value / 100;
    }
    document.getElementById("eye2TranslationSlider").oninput = function () {
        eye2Translation = document.getElementById("eye2TranslationSlider").value / 100;
    }

    document.getElementById("firstEarsRotationSlider").oninput = function () {
        firstEarsAngle = document.getElementById("firstEarsRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("secondEarsRotationSlider").oninput = function () {
        secondEarsAngle = document.getElementById("secondEarsRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("firstLegBaseRotationSlider").oninput = function () {
        firstLegBaseAngle = document.getElementById("firstLegBaseRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("secondLegBaseRotationSlider").oninput = function () {
        secondLegBaseAngle = document.getElementById("secondLegBaseRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("thirdLegBaseRotationSlider").oninput = function () {
        thirdLegBaseAngle = document.getElementById("thirdLegBaseRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("fourthLegBaseRotationSlider").oninput = function () {
        fourthLegBaseAngle = document.getElementById("fourthLegBaseRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("tailBaseRotationSlider").oninput = function () {
        tailBaseAngle = document.getElementById("tailBaseRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("tailTopRotationSlider").oninput = function () {
        tailTopAngle = document.getElementById("tailTopRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("baseHumanRotationSlider").oninput = function () {
        baseHumanAngle = document.getElementById("baseHumanRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("noseTranslationSlider").oninput = function () {
        humanNoseTranslation = document.getElementById("noseTranslationSlider").value / 100;
    }
    document.getElementById("humanEye1RotationSlider").oninput = function () {
        console.log(humanEye1Angle)
        humanEye1Angle = document.getElementById("humanEye1RotationSlider").value * Math.PI / 180;
    }
    document.getElementById("humanEye2RotationSlider").oninput = function () {
        console.log(humanEye2Angle)
        humanEye2Angle = document.getElementById("humanEye2RotationSlider").value * Math.PI / 180;
    }
    document.getElementById("humanLeg1RotationSlider").oninput = function () {
        console.log(humanLeg1Angle)
        humanLeg1Angle = document.getElementById("humanLeg1RotationSlider").value * Math.PI / 180;
    }
    document.getElementById("humanLeg2RotationSlider").oninput = function () {
        console.log(humanLeg2Angle)
        humanLeg2Angle = document.getElementById("humanLeg2RotationSlider").value * Math.PI / 180;
    }
    document.getElementById("humanArm1RotationSlider").oninput = function () {
        console.log(humanArm1Angle)
        humanArm1Angle = document.getElementById("humanArm1RotationSlider").value * Math.PI / 180;
    }
    document.getElementById("humanArm2RotationSlider").oninput = function () {
        console.log(humanArm2Angle)
        humanArm2Angle = document.getElementById("humanArm2RotationSlider").value * Math.PI / 180;
    }

    // ANT
    document.getElementById("baseAntRotationSlider").oninput = function () {
        baseAntAngle = document.getElementById("baseAntRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("firstLegRotationSlider").oninput = function () {
        firstLegAngle = document.getElementById("firstLegRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("secondLegRotationSlider").oninput = function () {
        secondLegAngle = document.getElementById("secondLegRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("thirdLegRotationSlider").oninput = function () {
        thirdLegAngle = document.getElementById("thirdLegRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("fourthLegRotationSlider").oninput = function () {
        fourthLegAngle = document.getElementById("fourthLegRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("fifthLegRotationSlider").oninput = function () {
        fifthLegAngle = document.getElementById("fifthLegRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("sixthLegRotationSlider").oninput = function () {
        sixthLegAngle = document.getElementById("sixthLegRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("headRotationSlider").oninput = function () {
        headAngle = document.getElementById("headRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("firstAnthenaRotationSlider").oninput = function () {
        firstAnthenaAngle = document.getElementById("firstAnthenaRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("secondAnthenaRotationSlider").oninput = function () {
        secondAnthenaAngle = document.getElementById("secondAnthenaRotationSlider").value * Math.PI / 180;
    }
    document.getElementById("stomachRotationSlider").oninput = function () {
        stomachAngle = document.getElementById("stomachRotationSlider").value * Math.PI / 180;
    }

    document.getElementById("dog-material").onchange = function () {
        dogMaterial = document.getElementById("dog-material").value;
    }
    document.getElementById("ant-material").onchange = function () {
        antMaterial = document.getElementById("ant-material").value;
    }
    document.getElementById("human-material").onchange = function () {
        humanMaterial = document.getElementById("human-material").value;
    }
    document.getElementById("room-material").onchange = function () {
        roomMaterial = document.getElementById("room-material").value;
    }
    document.getElementById("mode").onchange = function () {
        mode = document.getElementById("mode").value;
    }

    document.getElementById('camera_select').onchange = function () {
        camera_mode = document.getElementById('camera_select').value;
        if (camera_mode == 'default') {
            document.getElementById('cameraContent').style.display = 'block';
            cameraMode = true;
        }
        else {
            cameraMode = false;
            document.getElementById('cameraContent').style.display = 'none';
        }
    }

    
    document.getElementById('animal-select').onchange = function () {
        let option = document.getElementById('animal-select');
        let objectMode = option.value;
        let className = objectMode + "-control";
        let classObject = document.getElementsByClassName(className)[0];
        
        for(let i=0; i<option.length; i++) {
            let temp = option[i].value + "-control"
            let classTemp = document.getElementsByClassName(temp)[0];
            classTemp.classList.add("hidden-control");
        }

        classObject.classList.remove("hidden-control");
    }
}