function initTexture() {
    var image0 = new Image();
    image0.onload = function() {
       configureTexture(image0, gl.TEXTURE0);
    }
    image0.src = "img/arm_texture2.jpg"
    
    var image1 = new Image();
    image1.onload = function() {
       configureTexture(image1, gl.TEXTURE1);
    }
    image1.src = "img/wall2.jpg"
    
    var image2 = new Image();
    image2.onload = function() {
       configureTexture(image2, gl.TEXTURE2);
    }
    image2.src = "img/blue.jpg"
    
    var image3 = new Image();
    image3.onload = function() {
       configureTexture(image3, gl.TEXTURE3);
    }
    image3.src = "img/deep_blue.jpg"
    
    var image6 = new Image();
    image6.onload = function() {
       configureTexture(image6, gl.TEXTURE6);
    }
    image6.src = "img/black.jpg"
    
    var image7 = new Image();
    image7.onload = function() {
       configureTexture(image7, gl.TEXTURE7);
    }
    image7.src = "img/darkwood.jpg"
    
    var image8 = new Image();
    image8.onload = function() {
       configureTexture(image8, gl.TEXTURE8);
    }
    image8.src = "img/dalmantion.jpg"

    var image9 = new Image();
    image9.onload = function() {
       configureTexture(image9, gl.TEXTURE9);
    }
    image9.src = "img/eye.jpg"

    var image10 = new Image();
    image10.onload = function() {
       configureTexture(image10, gl.TEXTURE10);
    }
    image10.src = "img/dalmantion2.jpg"

    var image11 = new Image();
    image11.onload = function() {
       configureTexture(image11, gl.TEXTURE11);
    }
    image11.src = "img/ear.jpg"

    var image12 = new Image();
    image12.onload = function() {
       configureTexture(image12, gl.TEXTURE12);
    }
    image12.src = "img/tree.jpg"

    var image13 = new Image();
    image13.onload = function() {
       configureTexture(image13, gl.TEXTURE13);
    }
    image13.src = "img/redCloth.jpg"
}

function configureTexture(image, textureno) {
    var texture = gl.createTexture();
    gl.activeTexture(textureno);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D( gl.TEXTURE_2D, 0, gl.RGB,
         gl.RGB, gl.UNSIGNED_BYTE, image );
    gl.generateMipmap( gl.TEXTURE_2D );
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,
                      gl.NEAREST_MIPMAP_LINEAR );
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST );
    
}
